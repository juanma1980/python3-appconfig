# python3-qappconf

Python module that gives a skeleton for python graphical (pyside) apps forked from [LliureX python3-appconfig] (https://github.com/lliurex/python3-appconfig))

## Classes

### appConfig
**Methods**

1. appconfig()
2. set_baseDirs(str folders)
    Set the config folders for the app
3. set_configFile(str file)
    Set the name of the config file
4. get_configFile([str level])
    Returns the name of the config file for level (optional)
    return Type: str
5. set_defaultConfig(dict config)
    Set default values for config
6. set_level(str level)
    Set level. level could be "user" or "system"
7. getLevel()
    Return the current level
    return type:str
8. getConfig(str optional level, list optional exclude)
    Returns config for specified level if any, otherwise all the configuration
    return Type: Dict {level:{config},level2:{config}}
9. write_config(dict data, optional str level, optional str key, optional bool pk, optional bool create )
    Writes data in config file(s)

### appConfigScreen
**Methods**

1. appConfigScreen()
2. setwiki(str url)
    Sets url for the wiki (or help) page
3. hideNavMenu(bool hide)
    Shows or hides the navigation menu
4. setTextDomain(str textDomain)
    Sets the text domain for gettext
5. setRsrcPath(str path)
    Use path as base for resources
6. setIcon(str icon)
    Set icon app to icon
7. setBanner(str banner)
    Set banner to banner
8. setBackgroundImage(str img)
    Sets the background image for the startup page
9. setConfig(list folders,str file)
    Use folders as base folders for levels and file as config file
10. getConfig(str optional level, list optional exckude)
    See getConfig from appconfig
11. Show()
    Shows the app
12. gotoStack(int index,list args)
    Shows stack index
13. loadStack(int index, list args)
    Load stack index

### appConfigStack
**Methods**
1. appConfigStack()
2. initScreen()
3. setAppConfig(dict appconfig)
    Use appconfig dict as config
4. translate() deprecated
5. setTextDomain(str textDomain)
    See appConfigScreen
6. getConfig(optional str level, optional list exclude)

    See appConfig
7. setConfig(dict appconfig)
    Updates config if needed
8. setLevel(str level)
    Sets config level to level
9. updateScreen()
    Overloaded function
10. saveChanges(str key, vary data, optional str level)
    Writes key->data to config file for level
11. writeConfig()
    Overloaded function
12. hideControlButtons()
    Hides apply/cancel buttons
13. setChanged()
    Sets stack as modified
14. getChanges()
    Returns changes made to config
15. showMsg(str msg)
    Sends a notify

