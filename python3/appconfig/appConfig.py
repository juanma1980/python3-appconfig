#!/usr/bin/env python3
import sys
import os
import json
import tempfile
import base64
import subprocess
#from appconfig.appConfigN4d import appConfigN4d

class appConfig():
	def __init__(self):
		self.dbg=False
		self.confFile="appconfig.conf"
		self.home=os.environ.get('HOME',"/usr/share/{}".format(self.confFile.split('.')[0]))
		self.localConf=self.confFile
		self.baseDirs={"user":"{}/.config".format(self.home),"system":"/usr/share/{}".format(self.confFile.split('.')[0])}
		self.config={'user':{},'system':{}}
		self.server="172.20.9.174"
	#def __init__

	def _debug(self,msg):
		if self.dbg:
			print("Config: {}".format(msg))
	#def _debug

	def set_baseDirs(self,dirs):
		self.baseDirs=dirs.copy()
		self._debug("baseDirs: %s"%self.baseDirs)
	#def set_baseDirs

	def set_configFile(self,confFile):
		self.confFile=confFile
		self.localConf=self.confFile
		self._debug("ConfFile: %s"%self.confFile)
	#def set_confFile

	def get_configFile(self,level=None):
		confFile={}
		if level in self.baseDirs.keys():
			conf=os.path.join(self.baseDirs[level],self.confFile)
			confFile.update({level:conf})
		else:
			for level,item in self.baseDirs.items():
				conf=os.path.join(item,self.confFile)
				confFile.update({level:conf})
		return confFile
	#def get_configFile

	def set_defaultConfig(self,config):
		self.config.update({'default':config})
#		self._debug(self.config)
	#def set_defaultConfig

	def set_level(self,level):
		self.level=level
	#def set_level

	def getLevel(self):
		config=self.getConfig('system')
		level=config['system'].get('config','user')
		self.set_level(level)
		return(level)
	#def getLevel

	def getConfig(self,level=None,exclude=[]):
		self.config={'user':{},'system':{}}
		self.confFile=self.localConf

		if self.config[level]=={}:
			self.config[level]['config']=level
		config=self.config.copy()
		return (config)
	#def getConfig

	def _read_config_from_system(self,level=None,exclude=[]):
		def _read_file(confFile,level):
			data={}
			self._debug("Reading %s -> %s"%(confFile,level))
			if os.path.isfile(confFile):
				try:
					data=json.loads(open(confFile).read())
				except Exception as e:
					self._debug("Error opening %s: %s"%(confFile,e))
					
			if data:
				if not 'config' in data.keys():
					data['config']=level
				for excludeKey in exclude:
					if excludeKey in list(data.keys()):
						del data[excludeKey]
				self._debug("Updating %s -> %s"%(confFile,level))
				self.config.update({level:data})
		#def _read_file
		fileRead=False
		confFiles=self.get_configFile(level)
		for confLevel,confFile in confFiles.items():
			if os.path.isfile(confFile):
				fileRead=True
				_read_file(confFile,confLevel)
		return fileRead

	#def read_config_from_system

	def write_config(self,data,level=None,key=None,pk=None,create=True):
		self._debug("Writing key %s to %s Polkit:%s"%(key,level,pk))
		retval=True
		if level==None:
			level=self.getLevel()
		if level=='system' and not pk:
			self._debug("Invoking pk")
			try:
				data=json.dumps(data)
				subprocess.check_call(["pkexec","/usr/share/appconfig/auth/appconfig-polkit-helper.py",data,level,key,self.confFile,self.baseDirs[level]])
			except Exception as e:
				self._debug("Invoking pk failed: %s"%e)
				retval=False
		else:
			oldConf=self.getConfig(level)
#			self._debug("Old: %s"%oldConf)
			newConf=oldConf.copy()
			if key:
				if not level in newConf.keys():
					newConf[level]={key:None}
				if not key in newConf[level].keys():
					newConf[level][key]=None
				newConf[level][key]=data
			else:
				for key in data.keys():
					if not level in newConf.keys():
						newConf[level]={key:None}
					if not key in newConf[level].keys():
						newConf[level][key]=None
					newConf[level][key]=data[key]
			retval=self._write_config_to_system(newConf,level)
		return (retval)
	#def write_config

	def _write_config_to_system(self,conf,level='user'):
		data={}
		retval=True
		if not level in self.config.keys():
			self.config[level]={}
#		self._debug("Writing info %s"%self.config[level])
		if level and level in self.baseDirs.keys():
			confDir=self.baseDirs[level]
		else:
			confDir=self.defaultDir
		if not os.path.isdir(confDir):
			try:
				os.makedirs(confDir)
			except Exception as e:
				print("Can't create dir %s: %s"%(confDir,e))
				retval=False
		if retval:
			confFile=("%s/%s"%(confDir,self.confFile))
			self.config[level]=conf[level]
#			self._debug("New: %s"%self.config[level])
			try:
				with open(confFile,'w') as f:
					json.dump(self.config[level],f,indent=4,sort_keys=True)
			except Exception as e:
				retval=False
				print("Error writing system config: %s"%e)
		return (retval)
	#def _write_config_to_system

